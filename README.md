**About**

Oracle processes regions.xml.gz files from NationStates to predict whn a region will update. It uses user feedback to
adjust for observed variance to ensure later predictions are still accurate. Additionally, it can be used to generate
spreadsheets with region update times.

It consists of two classes:

- *Oracle* - Parses regions.xml.gz to predict the update time of a region
- *Delphi* - Extends Oracle with basic interactive functions

Oracle has been made open-source with the hopes of providing all players with the necessary technical tools to
participate in military gameplay.

**Usage**

You will need Python 3.0+ to use Oracle.

Delphi, which provides a basic interactive prompt, can be used to generate predictions. To run it, save a copy of
`regions.xml.gz` to the same directory and execute `delphi.py`. It will ask your for an identifier (nation name or email
address). Usage is as follows:

- `t <region>` - get region time
- `m <major|minor>` - set update mode to major or minor
- `r` - recall last targeted  region
- `o <hh:mm:ss>` - indicate true update time in hours, minutes, and seconds after the start of the update
- `c <region> <hh:mm:ss>` - calibrate update speed based on time of late updating region
- `export <filename>` - export CSV of oracle data using current update mode
- `targets <filename>` - export CSV of oracle data for founderless regions using current update mode
- `html <filename>` - export HTML of oracle data using current update mode
- `reload` - reload regions.xml.gz and reset Oracle settings to default

*Important*: Oracle's accuracy will heavily degrade if there are game-side changes to the update speed. In order to
calibrate Oracle's generated speeds, you must run `c <region> <hh:mm:ss>` with a late updating region's true time. If
the game's true speed differs significantly from Oracle's default or calibrated speed, prediction accuracy will be very
poor!

Best results will occur if someone is consistently providing Oracle with true region update times, either gathered 
through update spotting or pre-placed triggers. Oracle does not automatically track the update at this time, though
this functionality is planned for the future.

**Methodology**

During an update, Nationstates proceeds through all regions in the order that they are listed in regions.xml.gz, and
updates each nation, one at a time. Based on the observed per-nation update speed (obtained by dividing the total
number of regions by the length of the update in seconds), a region's update time can be predicted by multiplying the 
number of nations that precede that region by the per-nation update speed.

Because of variance, these predictions are less accurate towards the end of the update. To account for this, Oracle
allows a user to provide observed update times for regions during the update, and offsets its predictions based on this
value.

*Example*: If region A is predicted to update at 32:15, but actually updates at 33:40, Oracle will calculate the 
difference between its prediction and the true time (85 seconds), and offset all future predictions by that amount.

**Terms of Service Compliance**

Presently, Oracle queries the API once during initialization, to identify founderless regions. All other features rely
solely on the regions.xml.gz file and user input. Nonetheless, users must provide a user-agent string that will be
included in all requests to Nationstates.

The user-agent string is formatted as follows:

> Oracle *version number* (developed by khronion@gmail.com, in use by *user identifier*)

**Licensing**

Oracle is licensed under the Gnu Public License v3.0. In short, this means you may reuse, redistribute, and modify the
code as you wish, but all modifications must also be placed under the same license. You may view the license at 
<http://www.gnu.org/licenses/gpl-3.0.en.html>.

**History**

Oracle was initially developed for The Crimson Empire, a small raiding organization formed in 2015. It was later adopted
by The Kingdom of Alexandria to support the operations of its gameplay military, the Royal Legion of Alexandria. 
Following revelations that a widely used update prediction tool used by many groups was in violation of the Nationstates
terms of service, it was made open source.